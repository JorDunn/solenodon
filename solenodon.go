package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"runtime"
	"time"

	"github.com/cloudflare/cloudflare-go"

	"github.com/levigross/grequests"
	log "github.com/sirupsen/logrus"
)

type zoneConfiguration struct {
	Zones []struct {
		Name    string `json:"name"`
		ID      string `json:"id"`
		Records []struct {
			Name string `json:"name"`
			ID   string `json:"id"`
		} `json:"records"`
	} `json:"zones"`
}

// If email & key and token are all filled out, it will default to token.
// This should be used to make sure that solenodon only has the rights needed to update dns records.
type configuration struct {
	Cloudflare struct {
		Key            string `json:"key"`
		Email          string `json:"email"`
		Token          string `json:"token"`
		UserServiceKey string `json:"user_service_key"`
	} `json:"cloudflare"`
	UpdateInterval int `json:"update_interval"`
}

const (
	confDir = "/etc/solenodon/"
	logDir  = "/var/log/solenodon/"
)

var zoneConf zoneConfiguration
var conf configuration
var api *cloudflare.API
var err error
var opsys string

// command line flag vars
var configured bool
var cloudflareEmail string
var cloudflareKey string
var cloudflareToken string
var cloudflareUserServiceKey string
var listIDs bool

func init() {
	log.SetFormatter(&log.TextFormatter{})
	log.SetOutput(os.Stdout)

	flag.StringVar(&cloudflareEmail, "email", "none", "Cloudflare email.")
	flag.StringVar(&cloudflareKey, "key", "none", "Cloudflare API key.")
	flag.StringVar(&cloudflareToken, "token", "none", "Cloudflare API token.")
	flag.StringVar(&cloudflareUserServiceKey, "userservicekey", "none", "Cloudflare user service key.")
	flag.BoolVar(&listIDs, "listids", false, "List the zones and records associated with an account.")
	flag.Parse()

	/*
		The first three auth methods are for use with cmd line flags and last one reads the config file.
	*/
	if len(cloudflareUserServiceKey) > 4 {
		conf.Cloudflare.UserServiceKey = cloudflareUserServiceKey
		api, err = cloudflare.NewWithUserServiceKey(conf.Cloudflare.UserServiceKey)
		if err != nil {
			log.Fatalln(err)
		}
		conf.UpdateInterval = 60
		readZoneFile()
		configured = true
	} else if len(cloudflareToken) > 4 && configured != true {
		conf.Cloudflare.Token = cloudflareToken
		api, err = cloudflare.NewWithAPIToken(conf.Cloudflare.Token)
		if err != nil {
			log.Fatalln(err)
		}
		conf.UpdateInterval = 60
		readZoneFile()
		configured = true
	} else if len(cloudflareEmail) > 4 && len(cloudflareKey) > 4 && configured != true {
		conf.Cloudflare.Email = cloudflareEmail
		conf.Cloudflare.Key = cloudflareKey
		api, err = cloudflare.New(conf.Cloudflare.Key, conf.Cloudflare.Email)
		if err != nil {
			log.Fatalln(err)
		}
		conf.UpdateInterval = 60
		readZoneFile()
		configured = true
	} else if configured != true {
		readConfigFile()
		api, err = cloudflare.New(conf.Cloudflare.Key, conf.Cloudflare.Email)
		if err != nil {
			log.Fatalln(err)
		}
		readZoneFile()
		configured = true

	}
}

func readZoneFile() {
	/*
		Read the zones file to know what zones to update. Json file with zone and record ids.
	*/
	var zoneConfigFile *os.File

	if runtime.GOOS == "darwin" {
		zoneConfigFile, err = os.Open("/usr/local/etc/solenodon/zones.json")
	} else if runtime.GOOS == "linux" {
		zoneConfigFile, err = os.Open("/etc/solenodon/zones.json")
	} else {
		zoneConfigFile, err = os.Open("zones.json")
	}

	if err == nil {
		decoder := json.NewDecoder(zoneConfigFile)
		zoneConf = zoneConfiguration{}
		err = decoder.Decode(&zoneConf)
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		log.Fatalln(err)
	}

	zoneConfigFile.Close()
}
func readConfigFile() {
	var solenodonConfigFile *os.File

	if runtime.GOOS == "darwin" {
		log.Infoln("Detected Darwin system, using config from /usr/local/etc/solenodon.")
		solenodonConfigFile, err = os.Open("/usr/local/etc/solenodon/solenodon.json")
	} else if runtime.GOOS == "linux" {
		log.Infoln("Detected Linux system, using config from /usr/local/etc/solenodon.")
		solenodonConfigFile, err = os.Open("/etc/solenodon/solenodon.json")
	} else {
		log.Infoln("Could not detect system os, using config from local dir.")
		solenodonConfigFile, err = os.Open("solenodon.json")
	}

	if err == nil {
		decoder := json.NewDecoder(solenodonConfigFile)
		conf = configuration{}
		err = decoder.Decode(&conf)
		if err != nil {
			log.Fatalln(err)
		}
	} else {
		log.Fatalln(err)
	}

	solenodonConfigFile.Close()
}

func getIP() (resp *grequests.Response, err error) {
	resp, err = grequests.Get("https://api.ipify.org", nil)
	return resp, err
}

func updateDNS(ipAddress string, zoneconf zoneConfiguration) {
	for d := 0; d < len(zoneconf.Zones); d++ {
		// outer loop for zones
		log.WithFields(log.Fields{
			"name": zoneconf.Zones[d].Name,
			"id":   zoneconf.Zones[d].ID,
		}).Infoln("Updating", zoneconf.Zones[d].Name)
		for s := 0; s < len(zoneconf.Zones[d].Records); s++ {
			// inner loop for records
			log.WithFields(log.Fields{
				"subdomain": zoneconf.Zones[d].Records[s].Name,
				"id":        zoneconf.Zones[d].Records[s].ID,
			}).Infoln("⮑ ", zoneconf.Zones[d].Records[s].Name)
			// Instead of creating a whole new record just take current record, check the Contents (i.e. ip address), and if
			// they are different, update them.
			record, err := api.DNSRecord(zoneconf.Zones[d].ID, zoneconf.Zones[d].Records[s].ID)
			if err == nil {
				if record.Name == zoneconf.Zones[d].Records[s].Name {
					if record.Content != ipAddress {
						record.Content = ipAddress
						err = api.UpdateDNSRecord(zoneconf.Zones[d].ID, zoneconf.Zones[d].Records[s].ID, record)
						if err != nil {
							log.Errorln(err)
						}
					} else {
						log.Warnln("No changes detected for this record.")
					}
				} else {
					// This is to catch configuration errors. It wouldn't really effect anything though.
					log.Errorln("Record name and configured name do not match. Please fix config and restart.")
				}
			} else {
				log.Fatalln(err)
			}
		}
	}
}

func listZones() {
	// This is used for zone configuration. Lists all zones and records on the account.
	// TODO: Allow user to specify a certain zone. Useful for accounts with many zones and records.
	z, _ := api.ListZones()
	for i := 0; i < len(z); i++ {
		fmt.Println("* ", z[i].Name, z[i].ID)
		rr := cloudflare.DNSRecord{}
		r, _ := api.DNSRecords(z[i].ID, rr)
		for k := 0; k < len(r); k++ {
			fmt.Println("\t⮑ ", r[k].Type, r[k].Name, r[k].ID)
		}
	}
}

func main() {
	if listIDs == true {
		listZones()
	} else {
		var oldIP = "0.0.0.0"
		log.Infoln("Checking for changes every", conf.UpdateInterval, "seconds.")
		for true {
			resp, err := getIP()
			if err != nil {
				log.Errorln(err)
			}
			newIP := resp.String()
			if newIP != oldIP {
				log.WithFields(log.Fields{
					"old_ip": oldIP,
					"new_ip": newIP,
				}).Infoln("New IP address detected")
				updateDNS(newIP, zoneConf)
				oldIP = newIP
			} else {
				log.Infoln("No changes detected")
			}
			time.Sleep(time.Duration(conf.UpdateInterval) * time.Second)
		}
	}
}
