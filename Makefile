PROJECT_NAME := "solenodon"
PKG := "gitlab.com/JorDunn/$(PROJECT_NAME)"
UNAME := $(shell uname -s)

all:
	go build solenodon.go

deps:
	go get github.com/cloudflare/cloudflare-go
	go get github.com/levigross/grequests

deps-dev:
	go get github.com/cloudflare/cloudflare-go
	go get github.com/levigross/grequests
	go get github.com/sirupsen/logrus
	go get -u golang.org/x/lint/golint

install:
ifeq ($(UNAME),Darwin)
		cp solenodon /usr/local/bin/
		cp com.nodetwo.solenodon.plist /Users/$(USER)/Library/LaunchAgents/
		mkdir -p /usr/local/etc/solenodon/
		cp zones.json /usr/local/etc/solenodon
		cp solenodon.json /usr/local/etc/solenodon
endif
ifeq ($(UNAME),Linux)
		cp solenodon /usr/bin/
		mkdir -p /etc/systemd/system/solenodon.service.d
		cp solenodon.service /etc/systemd/system/solenodon.service.d/
		mkdir -p /etc/solenodon/
		cp {zones,solenodon}.json /etc/solenodon/
endif

uninstall:
ifeq ($(UNAME),Darwin)
		rm /usr/local/bin/solenodon
		rm /Users/$(USER)/Library/LaunchAgents/com.nodetwo.solenodon.plist
endif
ifeq ($(UNAME),Linux)
		rm /usr/bin/solenodon
		rm -r /etc/systemd/service/solenodon.service.d
endif