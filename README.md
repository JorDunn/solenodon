[![pipeline status](https://gitlab.com/JorDunn/solenodon/badges/master/pipeline.svg)](https://gitlab.com/JorDunn/solenodon/commits/master)
[![coverage report](https://gitlab.com/JorDunn/solenodon/badges/master/coverage.svg)](https://gitlab.com/JorDunn/solenodon/commits/master)
# Solenodon

Solenodon is v2 of my dynamic dns client/daemon. I am rewriting it in Go to decrease the memory/cpu footprint and to make it easier to provide binaries for various operating systems. The main driving force behind this rewrite was that using dynip used a lot of resources on my old servers, which where basically old PCs. This program would also work well with some VPS/server providers that provide dynamic ip addresses.

## TODO

- [x] Read zones from a json file.
- [ ] systemd integration.
- [ ] Look into dbus integration.
- [ ] Read `zones.json` and `solenodon.json` from `/etc/solenodon/` and `/usr/local/etc/solenodon/`.
- [ ] macOS launchd integration.
- [ ] Create .rb file for homebrew installation.
- [ ] Create .deb and .rpm files for linux installation.
- [ ] Possibly create Windows .msi installer with some sort of service integration.